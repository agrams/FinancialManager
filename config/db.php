<?php

return [
    'development' =>[
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'financial_manager',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci'
    ]
];