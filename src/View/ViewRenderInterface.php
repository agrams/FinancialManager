<?php
/**
 * Created by PhpStorm.
 * User: amanda
 * Date: 17/05/17
 * Time: 13:56
 */
declare(strict_types=1);
namespace MANFin\View;

use Psr\Http\Message\ResponseInterface;

interface ViewRenderInterface
{
    public function render(string $template, array $context = []):ResponseInterface;
}