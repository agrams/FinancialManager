<?php
/**
 * Created by PhpStorm.
 * User: amanda
 * Date: 17/05/17
 * Time: 14:00
 */

namespace MANFin\View;


use Psr\Http\Message\ResponseInterface;
use Twig_Environment;
use Zend\Diactoros\Response;

class ViewRenderer implements ViewRenderInterface
{
    /**
     * @var Twig_Environment
     */
    private $twigEnviroment;

    /**
     * ViewRenderer constructor.
     */
    public function __construct(Twig_Environment $twigEnviroment)
    {

        $this->twigEnviroment = $twigEnviroment;
    }

    public function render(string $template, array $context = []): ResponseInterface
    {
        $result = $this->twigEnviroment->render($template, $context);
        $response = new Response();
        $response->getBody()->write($result);
        return $response;
    }
}