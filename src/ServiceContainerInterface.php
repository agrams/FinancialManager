<?php
/**
 * Created by PhpStorm.
 * User: amanda
 * Date: 15/05/17
 * Time: 09:54
 */

declare(strict_types=1);
namespace MANFin;


interface ServiceContainerInterface
{
    public function add(string $name,$service);

    public function addLazy(string $name, callable $callable);

    public function get(string $name);

    public function has(string $name);
}