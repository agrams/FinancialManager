<?php
/**
 * Created by PhpStorm.
 * User: amanda
 * Date: 15/05/17
 * Time: 10:43
 */

namespace MANFin\Plugins;


use MANFin\ServiceContainerInterface;

interface PluginInterface
{

    public function register(ServiceContainerInterface $container);
}