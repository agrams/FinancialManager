<?php
/**
 * Created by PhpStorm.
 * User: amanda
 * Date: 15/05/17
 * Time: 11:09
 */
declare(strict_types=1);

namespace MANFin\Plugins;


use MANFin\ServiceContainerInterface;
use Illuminate\Database\Capsule\Manager as Capsule;

class DbPlugin implements PluginInterface
{
    public function register(ServiceContainerInterface $container)
    {
        $capsule = new Capsule();
        $config = include __DIR__ . "/../../config/db.php";
        $capsule->addConnection($config['development']);
        $capsule->bootEloquent();
    }

}