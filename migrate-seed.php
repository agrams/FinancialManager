<?php
/**
 * Created by PhpStorm.
 * User: amanda
 * Date: 15/05/17
 * Time: 09:10
 */

exec(__DIR__ .'/vendor/bin/phinx rollback -t 0');
exec(__DIR__ .'/vendor/bin/phinx migrate');
exec(__DIR__ .'/vendor/bin/phinx seed:run');